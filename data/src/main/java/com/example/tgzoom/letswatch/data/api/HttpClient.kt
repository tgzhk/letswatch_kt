package com.example.tgzoom.letswatch.data.api

/**
 * Created by tgzoom on 6/12/17.
 */
interface HttpClient {
    fun <T> create(service: Class<T>): T
}