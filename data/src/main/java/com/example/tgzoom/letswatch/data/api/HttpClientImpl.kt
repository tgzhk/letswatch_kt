package com.example.tgzoom.letswatch.data.api

import retrofit2.Retrofit

/**
 * Created by tgzoom on 6/12/17.
 */
class HttpClientImpl(val retrofit: Retrofit): HttpClient{
    override fun <T> create(service: Class<T>): T {
        return retrofit.create(service)
    }
}