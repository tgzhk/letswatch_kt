package com.example.tgzoom.letswatch.data.api

import com.example.tgzoom.letswatch.data.entity.MovieEntity
import com.example.tgzoom.letswatch.data.entity.PageEntity
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import rx.Observable

/**
 * Created by tgzoom on 6/5/17.
 */
interface MovieApi{
    @GET("3/discover/movie/{movie_id}")
    fun movie(@Path("movie_id") movieApiId: Long, @Query("api_key") api_key: String): Observable<MovieEntity>

    @GET("3/search/movie")
    fun search(@Query("query") query: String, @Query("page") page: Int, @Query("api_key") api_key: String): Observable<PageEntity>

    @GET("3/discover/movie/popular")
    fun popular(@Query("page") page: Int, @Query("api_key") api_key: String): Observable<PageEntity>

    @GET("3/discover/movie/top_rated")
    fun topRated(@Query("page") page: Int, @Query("api_key") api_key: String): Observable<PageEntity>
}