package com.example.tgzoom.letswatch.data.entity

import com.google.gson.annotations.SerializedName

/**
 * Created by tgzoom on 5/10/17.
 */
data class MovieEntity (
        @SerializedName("id")
        var id: Long = 0,

        @SerializedName("original_titleoriginal_title")
        var original_title: String = "",

        @SerializedName("title")
        var title: String = "",

        @SerializedName("vote_average")
        var vote_average: Double = 0.0,

        @SerializedName("vote_count")
        var vote_count: Double = 0.0,

        @SerializedName("backdrop_path")
        var backdrop_path: String = "",

        @SerializedName("genres_ids")
        var genres_ids: List<Int> = emptyList(),

        @SerializedName("release_date")
        var release_date: String = "",

        @SerializedName("overview")
        var overview: String = "",

        @SerializedName("popularity")
        var popularity: Double = 0.0,

        @SerializedName("original_language")
        var original_language: String = ""
)