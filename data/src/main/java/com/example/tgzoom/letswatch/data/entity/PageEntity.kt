package com.example.tgzoom.letswatch.data.entity

import com.google.gson.annotations.SerializedName

/**
 * Created by tgzoom on 6/6/17.
 */
class PageEntity (
        @SerializedName("page")
        var page: Int = 0,

        @SerializedName("total_results")
        var totalResults: String = "",

        @SerializedName("total_pages")
        var totalPages: String = "",

        @SerializedName("results")
        var results: List<MovieEntity>

)