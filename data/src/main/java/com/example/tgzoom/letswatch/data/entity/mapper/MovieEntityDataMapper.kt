package com.example.tgzoom.letswatch.data.entity.mapper

import com.example.tgzoom.letswatch.data.entity.MovieEntity
import com.example.tgzoom.letswatch.domain.Movie

/**
 * Created by tgzoom on 5/11/17.
 */
open class MovieEntityDataMapper {

    fun  map(movieEntity: MovieEntity): Movie {
        var movie = Movie(
            id = movieEntity.id,
            backdrop_path= movieEntity.backdrop_path,
            genres_ids = movieEntity.genres_ids,
            original_language = movieEntity.original_language,
            overview = movieEntity.overview,
            popularity = movieEntity.popularity,
            release_date = movieEntity.release_date,
            title = movieEntity.title,
            vote_average = movieEntity.vote_average,
            vote_count = movieEntity.vote_count,
            original_title = movieEntity.original_title
        )

        return movie
    }

    fun  map(movieEntityList: List<MovieEntity>): List<Movie> {
        var movieList = mutableListOf<Movie>()
        for (movieEntity in movieEntityList ){
            movieList.add(map(movieEntity))
        }
        return movieList
    }
}