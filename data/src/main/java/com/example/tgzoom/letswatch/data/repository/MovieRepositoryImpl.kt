package com.example.tgzoom.letswatch.data.repository

import com.example.tgzoom.letswatch.data.entity.mapper.MovieEntityDataMapper
import com.example.tgzoom.letswatch.data.repository.datasource.MovieDataSourceCache
import com.example.tgzoom.letswatch.data.repository.datasource.MovieDataSourceDisk
import com.example.tgzoom.letswatch.data.repository.datasource.MovieDataSourceNetwork
import com.example.tgzoom.letswatch.domain.Movie
import com.example.tgzoom.letswatch.domain.repository.MovieRepository
import io.reactivex.Observable

/**
 * Created by tgzoom on 5/15/17.
 */
class MovieRepositoryImpl(
        private val movieDataSourceCache: MovieDataSourceCache,
        private val movieDataSourceNetwork: MovieDataSourceNetwork,
        private val movieDataSourceDisk: MovieDataSourceDisk,
        private val movieEntityDataMapper:MovieEntityDataMapper
): MovieRepository {

    override fun popular(page: Int): Observable<List<Movie>> {
        return Observable
                .concat(movieDataSourceCache.popular(page),movieDataSourceDisk.popular(page),movieDataSourceNetwork.popular(page))
                .firstElement()
                .observable()
                .map(movieEntityDataMapper::map)
    }

    override fun topRated(page: Int): Observable<List<Movie>> {
        return Observable
                .concat(movieDataSourceCache.topRated(page),movieDataSourceDisk.topRated(page),movieDataSourceNetwork.topRated(page))
                .first()
                .map(movieEntityDataMapper::map)

    }

    override fun movie(movieApiId: Long): Observable<Movie> {
        return Observable
                .concat(movieDataSourceCache.movie(movieApiId),movieDataSourceDisk.movie(movieApiId),movieDataSourceNetwork.movie(movieApiId))
                .first()
                .map(movieEntityDataMapper::map)
    }

    override fun search(queryString: String, page: Int): Observable<List<Movie>> {
        return Observable
                .concat(movieDataSourceCache.search(queryString,page),movieDataSourceDisk.search(queryString,page),movieDataSourceNetwork.search(queryString,page))
                .first()
                .map(movieEntityDataMapper::map)
    }
}