package com.example.tgzoom.letswatch.data.repository.datasource

import com.example.tgzoom.letswatch.data.entity.MovieEntity
import rx.Observable

/**
 * Created by tgzoom on 5/15/17.
 */
interface MovieDataSource {
    fun popular(page: Int): Observable<List<MovieEntity>>
    fun topRated(page: Int): Observable<List<MovieEntity>>
    fun movie(movieApiId:Long): Observable<MovieEntity>
    fun search(queryString: String,page: Int): Observable<List<MovieEntity>>
}