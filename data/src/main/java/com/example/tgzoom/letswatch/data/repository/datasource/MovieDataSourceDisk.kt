package com.example.tgzoom.letswatch.data.repository.datasource

import com.example.tgzoom.letswatch.data.entity.MovieEntity
import rx.Observable

/**
 * Created by tgzoom on 5/15/17.
 */
open class MovieDataSourceDisk: MovieDataSource {

    override fun popular(page: Int): Observable<List<MovieEntity>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun topRated(page: Int): Observable<List<MovieEntity>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun movie(movieApiId: Long): Observable<MovieEntity> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun search(queryString: String,page: Int): Observable<List<MovieEntity>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}