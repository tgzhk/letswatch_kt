package com.example.tgzoom.letswatch.data.repository.datasource

import com.example.letswatch.data.BuildConfig
import com.example.tgzoom.letswatch.data.api.HttpClient
import com.example.tgzoom.letswatch.data.api.MovieApi
import com.example.tgzoom.letswatch.data.entity.MovieEntity
import com.example.tgzoom.letswatch.data.entity.PageEntity
import com.example.tgzoom.letswatch.domain.scheduler.BaseScheduler
import rx.Observable

/**
 * Created by tgzoom on 5/15/17.
 */
open class MovieDataSourceNetwork(val httpClient: HttpClient, val baseScheduler: BaseScheduler): MovieDataSource {
    override fun popular(page: Int): Observable<List<MovieEntity>> {
        var movieService = httpClient.create(MovieApi::class.java)
        return movieService.popular(page,BuildConfig.MOVIE_DB_API_KEY)
                .map {pageEntity:PageEntity -> pageEntity.results}
                .compose(baseScheduler.applySchedulers())
    }

    override fun topRated(page: Int): Observable<List<MovieEntity>> {
        var movieService = httpClient.create(MovieApi::class.java)
        return movieService.topRated(page,BuildConfig.MOVIE_DB_API_KEY)
                .map {pageEntity:PageEntity -> pageEntity.results}
                .compose(baseScheduler.applySchedulers())
    }

    override fun movie(movieApiId: Long): Observable<MovieEntity> {
        var movieService = httpClient.create(MovieApi::class.java)
        return movieService.movie(movieApiId,BuildConfig.MOVIE_DB_API_KEY)
                .compose(baseScheduler.applySchedulers())
    }

    override fun search(queryString: String,page: Int): Observable<List<MovieEntity>> {
        var movieService = httpClient.create(MovieApi::class.java)
        return movieService.search(queryString,page,BuildConfig.MOVIE_DB_API_KEY)
                .map {pageEntity:PageEntity -> pageEntity.results}
                .compose(baseScheduler.applySchedulers())
    }
}