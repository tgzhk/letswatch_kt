package com.example.tgzoom.letswatch.data.repository.factory

import com.example.tgzoom.letswatch.data.entity.mapper.MovieEntityDataMapper
import com.example.tgzoom.letswatch.data.repository.datasource.MovieDataSource

/**
 * Created by tgzoom on 5/15/17.
 */
interface MovieDataSourceFactory {
    fun topRated():MovieDataSource
    fun popular():MovieDataSource
    fun movie(): MovieDataSource
    fun search(): MovieDataSource
}