package com.example.tgzoom.letswatch.data.repository.factory

import com.example.tgzoom.letswatch.data.entity.mapper.MovieEntityDataMapper
import com.example.tgzoom.letswatch.data.repository.datasource.MovieDataSource

/**
 * Created by tgzoom on 5/15/17.
 */
class MovieDataSourceFactoryImpl: MovieDataSourceFactory{

    val movieEntityDataMapper = MovieEntityDataMapper()

    override fun topRated(): MovieDataSource {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun popular(): MovieDataSource {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun movie(): MovieDataSource {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun search(): MovieDataSource {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}