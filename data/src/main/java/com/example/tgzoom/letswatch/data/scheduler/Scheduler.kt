package com.example.tgzoom.letswatch.data.scheduler

import com.example.tgzoom.letswatch.domain.scheduler.BaseScheduler
import io.reactivex.MaybeTransformer
import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


/**
 * Created by tgzoom on 6/12/17.
 */
class Scheduler: BaseScheduler {

    lateinit private var INSTANCE: Scheduler

    @Synchronized fun getInstance(): Scheduler {
        if (INSTANCE == null) {
            INSTANCE = Scheduler()
        }
        return INSTANCE
    }

    override fun computation(): io.reactivex.Scheduler {
        return Schedulers.computation()
    }

    override fun io(): io.reactivex.Scheduler {
        return Schedulers.io()
    }

    override fun ui():  io.reactivex.Scheduler {
        return AndroidSchedulers.mainThread()
    }

    override fun <T> applyObservableSchedulers(): ObservableTransformer<T, T> {
        return ObservableTransformer { observable ->
            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
        }
    }

    override fun <T> applyMaybeSchedulers(): MaybeTransformer<T, T> {
        return MaybeTransformer { maybe ->
            maybe.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
        }
    }
}