package com.example.tgzoom.letswatch.test.entity.mapper

import com.example.tgzoom.letswatch.data.entity.MovieEntity
import com.example.tgzoom.letswatch.data.entity.mapper.MovieEntityDataMapper
import com.example.tgzoom.letswatch.domain.Movie
import org.junit.Assert
import org.junit.Before
import org.junit.Test

/**
 * Created by tgzoom on 5/11/17.
 */
class MovieEntityDataMapperTest {

    val movieEntityDataMapper = MovieEntityDataMapper()
    val FAKE_ID = 305L
    val FAKE_BACKDROP_PATH = "backdrop_test"
    val FAKE_GENRES_IDS = mutableListOf(1,2)
    val FAKE_ORIGINAL_LANGUAGE = "test"
    val FAKE_OVERVIEW = "overview_test"
    val FAKE_POPULARITY = 3.5
    val FAKE_RELEASE_DATE = "2017-05-09"
    val FAKE_TITLE = "title_test"
    val FAKE_VOTE_AVERAGE = 4.5
    val FAKE_VOTE_COUNT = 8.9
    val FAKE_ORIGINAL_TITLE = "original_title_test"
    lateinit var fakeMovieEntity: MovieEntity
    lateinit var fakeMovieEntityList: MutableList<MovieEntity>
    lateinit var fakeMovie: Movie
    lateinit var fakeMovieList: MutableList<Movie>

    @Before
    fun setUp(){
        fakeMovieEntity = MovieEntity(
                id = FAKE_ID,
                backdrop_path= FAKE_BACKDROP_PATH,
                genres_ids = FAKE_GENRES_IDS,
                original_language = FAKE_ORIGINAL_LANGUAGE,
                overview = FAKE_OVERVIEW,
                popularity = FAKE_POPULARITY,
                release_date = FAKE_RELEASE_DATE,
                title = FAKE_TITLE,
                vote_average = FAKE_VOTE_AVERAGE,
                vote_count = FAKE_VOTE_COUNT,
                original_title = FAKE_ORIGINAL_TITLE
        )

        fakeMovie = Movie(
                id = FAKE_ID,
                backdrop_path= FAKE_BACKDROP_PATH,
                genres_ids = FAKE_GENRES_IDS,
                original_language = FAKE_ORIGINAL_LANGUAGE,
                overview = FAKE_OVERVIEW,
                popularity = FAKE_POPULARITY,
                release_date = FAKE_RELEASE_DATE,
                title = FAKE_TITLE,
                vote_average = FAKE_VOTE_AVERAGE,
                vote_count = FAKE_VOTE_COUNT,
                original_title = FAKE_ORIGINAL_TITLE
        )

        fakeMovieEntityList = mutableListOf(MovieEntity(FAKE_ID), MovieEntity(FAKE_ID))
        fakeMovieList = mutableListOf(Movie(FAKE_ID), Movie(FAKE_ID))
    }

    @Test
    fun serializeToSingleObject(){
        var movie = movieEntityDataMapper.map(fakeMovieEntity)
        Assert.assertEquals(movie,fakeMovie)
    }

    @Test
    fun serializeToObjectList(){
        var movieList = movieEntityDataMapper.map(fakeMovieEntityList)
        Assert.assertEquals(movieList,fakeMovieList)
    }

}