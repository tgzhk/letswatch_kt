package com.example.tgzoom.letswatch.test.repository

import com.example.letswatch.data.BuildConfig
import com.example.tgzoom.letswatch.data.api.HttpClient
import com.example.tgzoom.letswatch.data.api.MovieApi
import com.example.tgzoom.letswatch.data.entity.MovieEntity
import com.example.tgzoom.letswatch.data.repository.datasource.MovieDataSourceNetwork
import com.example.tgzoom.letswatch.domain.scheduler.BaseScheduler
import org.junit.Before
import org.junit.Test
import org.mockito.BDDMockito
import org.mockito.*
import org.mockito.MockitoAnnotations
import rx.Observable
import rx.schedulers.TestScheduler


/**
 * Created by tgzoom on 6/5/17.
 */

class MovieDataSourceNetworkTest{
    @Mock lateinit var httpClient: HttpClient
    @Mock lateinit var baseScheduler: BaseScheduler
    @Mock lateinit var movieService: MovieApi

    @InjectMocks lateinit var movieDataSourceNetwork: MovieDataSourceNetwork
    private val FAKE_MOVIE_ID = 10L
    private val FAKE_QUERY_SEARCH = ""
    private val FAKE_PAGE = 1
    private val MOVIE_DB_KEY = BuildConfig.MOVIE_DB_API_KEY

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        BDDMockito.given(httpClient.create(MovieApi::class.java)).willReturn(movieService)
        BDDMockito.given(movieService.movie(FAKE_MOVIE_ID, MOVIE_DB_KEY)).willReturn(Observable.empty())
        BDDMockito.given(movieService.topRated(FAKE_PAGE, MOVIE_DB_KEY)).willReturn(Observable.empty())
        BDDMockito.given(movieService.popular(FAKE_PAGE, MOVIE_DB_KEY)).willReturn(Observable.empty())
        BDDMockito.given(movieService.search(FAKE_QUERY_SEARCH,FAKE_PAGE, MOVIE_DB_KEY)).willReturn(Observable.empty())
        BDDMockito.given(baseScheduler.applySchedulers<Observable<MovieEntity>>()).willReturn(rx.Observable.Transformer {
            observable -> observable.subscribeOn(TestScheduler())
                            .observeOn(TestScheduler()) }
        )
    }

    @Test
    fun getPopularMoviesFromNetwork(){
        var movieEntity = movieDataSourceNetwork.popular(FAKE_PAGE)
        Mockito.verify(movieService).popular(FAKE_PAGE,MOVIE_DB_KEY)
        checkNotNull(movieEntity)
    }

    @Test
    fun getTopRatedMoviesFromNetwork(){
        var movieEntity = movieDataSourceNetwork.topRated(FAKE_PAGE)
        Mockito.verify(movieService).topRated(FAKE_PAGE,MOVIE_DB_KEY)
        checkNotNull(movieEntity)
    }

    @Test
    fun getSearchedMoviesFromNetwork(){
        var movieEntity = movieDataSourceNetwork.search(FAKE_QUERY_SEARCH,FAKE_PAGE)
        Mockito.verify(movieService).search(FAKE_QUERY_SEARCH,FAKE_PAGE,MOVIE_DB_KEY)
        checkNotNull(movieEntity)
    }

    @Test
    fun getMovieFromNetwork(){
        var movieEntity = movieDataSourceNetwork.movie(FAKE_MOVIE_ID)
        Mockito.verify(movieService).movie(FAKE_MOVIE_ID,MOVIE_DB_KEY)
        checkNotNull(movieEntity)
    }
}