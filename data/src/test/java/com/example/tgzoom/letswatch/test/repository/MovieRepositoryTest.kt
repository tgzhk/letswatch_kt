package com.example.tgzoom.letswatch.test.repository

import com.example.tgzoom.letswatch.data.entity.mapper.MovieEntityDataMapper
import com.example.tgzoom.letswatch.data.repository.MovieRepositoryImpl
import com.example.tgzoom.letswatch.data.repository.datasource.MovieDataSource
import com.example.tgzoom.letswatch.data.repository.datasource.MovieDataSourceCache
import com.example.tgzoom.letswatch.data.repository.datasource.MovieDataSourceDisk
import com.example.tgzoom.letswatch.data.repository.datasource.MovieDataSourceNetwork
import org.junit.Before
import org.junit.Test
import org.mockito.*
import org.mockito.BDDMockito.*
import rx.Observable

/**
 * Created by tgzoom on 5/15/17.
 */
class MovieRepositoryTest {

    private val FAKE_MOVIE_API_ID = 305L
    private val FAKE_MOVIE_QUERY_STRING = "teste"
    private val FAKE_PAGE = 1
    @Mock lateinit var movieEntityDataMapper: MovieEntityDataMapper
    @Mock lateinit var movieDataSourceDisk: MovieDataSourceDisk
    @Mock lateinit var movieDataSourceNetwork: MovieDataSourceNetwork
    @Mock lateinit var movieDataSourceCache: MovieDataSourceCache
    @InjectMocks lateinit var movieRepositoryImp: MovieRepositoryImpl

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)

        given(movieDataSourceDisk.search(FAKE_MOVIE_QUERY_STRING,FAKE_PAGE)).willReturn(Observable.empty())
        given(movieDataSourceDisk.popular(FAKE_PAGE)).willReturn(Observable.empty())
        given(movieDataSourceDisk.topRated(FAKE_PAGE)).willReturn(Observable.empty())
        given(movieDataSourceDisk.movie(FAKE_MOVIE_API_ID)).willReturn(Observable.empty())

        given(movieDataSourceNetwork.search(FAKE_MOVIE_QUERY_STRING,FAKE_PAGE)).willReturn(Observable.empty())
        given(movieDataSourceNetwork.popular(FAKE_PAGE)).willReturn(Observable.empty())
        given(movieDataSourceNetwork.topRated(FAKE_PAGE)).willReturn(Observable.empty())
        given(movieDataSourceNetwork.movie(FAKE_MOVIE_API_ID)).willReturn(Observable.empty())

        given(movieDataSourceCache.search(FAKE_MOVIE_QUERY_STRING,FAKE_PAGE)).willReturn(Observable.empty())
        given(movieDataSourceCache.popular(FAKE_PAGE)).willReturn(Observable.empty())
        given(movieDataSourceCache.topRated(FAKE_PAGE)).willReturn(Observable.empty())
        given(movieDataSourceCache.movie(FAKE_MOVIE_API_ID)).willReturn(Observable.empty())
    }

    @Test
    fun gettingMoviesCorrectly(){
        movieRepositoryImp.movie(FAKE_MOVIE_API_ID)
    }

    @Test
    fun movieFunctionBeingCalled(){
        movieRepositoryImp.movie(FAKE_MOVIE_API_ID)
        Mockito.verify(movieDataSourceDisk).movie(FAKE_MOVIE_API_ID)
        Mockito.verify(movieDataSourceNetwork).movie(FAKE_MOVIE_API_ID)
        Mockito.verify(movieDataSourceCache).movie(FAKE_MOVIE_API_ID)
    }

    @Test
    fun topRatedFunctionBeingCalled(){
        movieRepositoryImp.topRated(FAKE_PAGE)
        Mockito.verify(movieDataSourceDisk).topRated(FAKE_PAGE)
        Mockito.verify(movieDataSourceNetwork).topRated(FAKE_PAGE)
        Mockito.verify(movieDataSourceCache).topRated(FAKE_PAGE)
    }

    @Test
    fun popularFunctionBeingCalled(){
        movieRepositoryImp.popular(FAKE_PAGE)
        Mockito.verify(movieDataSourceDisk).popular(FAKE_PAGE)
        Mockito.verify(movieDataSourceNetwork).popular(FAKE_PAGE)
        Mockito.verify(movieDataSourceCache).popular(FAKE_PAGE)
    }

    @Test
    fun searchFunctionBeingCalled(){
        movieRepositoryImp.search(FAKE_MOVIE_QUERY_STRING,FAKE_PAGE)
        Mockito.verify(movieDataSourceDisk).search(FAKE_MOVIE_QUERY_STRING,FAKE_PAGE)
        Mockito.verify(movieDataSourceNetwork).search(FAKE_MOVIE_QUERY_STRING,FAKE_PAGE)
        Mockito.verify(movieDataSourceCache).search(FAKE_MOVIE_QUERY_STRING,FAKE_PAGE)
    }
}