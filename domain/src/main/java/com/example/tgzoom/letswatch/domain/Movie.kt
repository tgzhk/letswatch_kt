package com.example.tgzoom.letswatch.domain

/**
 * Created by tgzoom on 4/22/17.
 */
data class Movie(
        var id: Long = 0,
        var original_title: String = "",
        var title: String = "",
        var vote_average: Double = 0.0,
        var vote_count: Double = 0.0,
        var backdrop_path: String = "",
        var genres_ids: List<Int> = emptyList(),
        var release_date: String = "",
        var overview: String = "",
        var popularity: Double = 0.0,
        var original_language: String = ""
)