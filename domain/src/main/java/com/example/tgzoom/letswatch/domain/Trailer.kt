package com.example.tgzoom.letswatch.domain

/**
 * Created by tgzoom on 4/27/17.
 */
data class Trailer(
        var id:String = "",
        var key:String = "",
        var name:String = "",
        var type:String = "",
        var size:String = "",
        var site:String = ""
)