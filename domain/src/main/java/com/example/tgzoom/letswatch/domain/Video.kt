package com.example.tgzoom.letswatch.domain

/**
 * Created by tgzoom on 4/22/17.
 */
data class Video(
        var id: String = "",
        var key: String = "",
        var name: String = "",
        var site: String = "",
        var size: String = "",
        var type: String = ""
)