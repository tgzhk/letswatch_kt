package com.example.tgzoom.letswatch.domain.interactor

import com.example.tgzoom.letswatch.domain.Movie
import com.example.tgzoom.letswatch.domain.repository.MovieRepository
import com.example.tgzoom.letswatch.domain.scheduler.BaseScheduler
import io.reactivex.MaybeObserver

/**
 * Created by tgzoom on 4/27/17.
 */
open class GetMovieDetailsUseCase(
        baseScheduler:BaseScheduler,
        val movieRepository: MovieRepository
): UseCase<GetMovieDetailsUseCase.Request>(baseScheduler) {

    override fun executeUseCase(request: Request) {
        movieRepository
                .movie(request.movieApiId)
                .compose(baseScheduler.applyMaybeSchedulers())
                .subscribeWith(request.observer)
    }

    class Request(val movieApiId: Long, val observer: MaybeObserver<Movie>) : UseCase.Request
}