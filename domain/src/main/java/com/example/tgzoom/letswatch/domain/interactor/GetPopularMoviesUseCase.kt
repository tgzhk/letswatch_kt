package com.example.tgzoom.letswatch.domain.interactor

import com.example.tgzoom.letswatch.domain.Movie
import com.example.tgzoom.letswatch.domain.repository.MovieRepository
import com.example.tgzoom.letswatch.domain.scheduler.BaseScheduler
import io.reactivex.MaybeObserver


/**
 * Created by tgzoom on 4/26/17.
 */
open class GetPopularMoviesUseCase(
        baseScheduler: BaseScheduler,
        val movieRespository: MovieRepository
    ): UseCase<GetPopularMoviesUseCase.Request>(baseScheduler) {

    override fun executeUseCase(request: Request) {
        movieRespository
                .popular(request.page)
                .compose(baseScheduler.applyMaybeSchedulers())
                .subscribeWith(request.observer)
    }

    class Request(val page:Int, val observer: MaybeObserver<List<Movie>>) : UseCase.Request
}