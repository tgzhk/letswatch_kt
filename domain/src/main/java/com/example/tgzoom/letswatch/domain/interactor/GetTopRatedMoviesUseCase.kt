package com.example.tgzoom.letswatch.domain.interactor

import com.example.tgzoom.letswatch.domain.Movie
import com.example.tgzoom.letswatch.domain.repository.MovieRepository
import com.example.tgzoom.letswatch.domain.scheduler.BaseScheduler
import io.reactivex.MaybeObserver

/**
 * Created by tgzoom on 5/8/17.
 */
class GetTopRatedMoviesUseCase(
        baseScheduler: BaseScheduler,
        val movieRepository: MovieRepository
): UseCase<GetTopRatedMoviesUseCase.Request>(baseScheduler)  {

    override fun executeUseCase(request: Request) {
        movieRepository
                .topRated(request.page)
                .compose(baseScheduler.applyMaybeSchedulers())
                .subscribeWith(request.observer)
    }

    class Request(val page: Int, val observer: MaybeObserver<List<Movie>>) : UseCase.Request
}