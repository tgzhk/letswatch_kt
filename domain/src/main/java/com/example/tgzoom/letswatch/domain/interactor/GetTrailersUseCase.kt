package com.example.tgzoom.letswatch.domain.interactor

import com.example.tgzoom.letswatch.domain.Trailer
import com.example.tgzoom.letswatch.domain.repository.TrailerRepository
import com.example.tgzoom.letswatch.domain.scheduler.BaseScheduler
import io.reactivex.MaybeObserver

/**
 * Created by tgzoom on 5/4/17.
 */
class GetTrailersUseCase(
        baseScheduler: BaseScheduler,
        val trailerRepository: TrailerRepository
): UseCase<GetTrailersUseCase.Request>(baseScheduler){

    override fun executeUseCase(request: Request) {
        TODO()
    }

    class Request(val movieApiId: Long, val observer: MaybeObserver<List<Trailer>>) : UseCase.Request
}
