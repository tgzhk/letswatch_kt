package com.example.tgzoom.letswatch.domain.interactor

/**
 * Created by tgzoom on 4/25/17.
 */
interface Interactor<Request> {
    fun execute(request:Request)
}