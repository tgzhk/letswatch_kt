package com.example.tgzoom.letswatch.domain.interactor

import com.example.tgzoom.letswatch.domain.Movie
import com.example.tgzoom.letswatch.domain.repository.MovieRepository
import com.example.tgzoom.letswatch.domain.scheduler.BaseScheduler
import io.reactivex.MaybeObserver

/**
 * Created by tgzoom on 5/1/17.
 */
class SearchMoviesUseCase(
        baseScheduler: BaseScheduler,
        val movieRepository: MovieRepository
): UseCase<SearchMoviesUseCase.Request>(baseScheduler) {

    override fun executeUseCase(request: Request) {
        movieRepository
                .search(request.queryString,request.page)
                .compose(baseScheduler.applyMaybeSchedulers())
                .subscribeWith(request.observer)
    }
    class Request(val queryString: String, val page:Int, val observer: MaybeObserver<List<Movie>>) : UseCase.Request
}