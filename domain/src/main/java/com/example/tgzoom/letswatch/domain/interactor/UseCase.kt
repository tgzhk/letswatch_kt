package com.example.tgzoom.letswatch.domain.interactor

import com.example.tgzoom.letswatch.domain.scheduler.BaseScheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by tgzoom on 4/23/17.
 */
abstract class UseCase<Request>(val baseScheduler: BaseScheduler): Interactor<Request> {
    var mDisposable = CompositeDisposable()

    protected abstract fun executeUseCase(request: Request)

    override fun execute(request: Request) {
        return executeUseCase(request)
    }

    fun addSubscription(disposable:Disposable): Unit{
        mDisposable.add(disposable)
    }

     fun dispose():Unit{
        if(!mDisposable.isDisposed()){
            mDisposable.dispose()
        }
    }

    interface Request{}
}
