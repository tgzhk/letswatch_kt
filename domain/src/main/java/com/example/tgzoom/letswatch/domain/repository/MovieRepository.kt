package com.example.tgzoom.letswatch.domain.repository

import com.example.tgzoom.letswatch.domain.Movie
import io.reactivex.Observable

/**
 * Created by tgzoom on 4/26/17.
 */
interface MovieRepository {
    fun popular(page: Int): Observable<List<Movie>>
    fun topRated(page: Int): Observable<List<Movie>>
    fun movie(movieApiId:Long): Observable<Movie>
    fun search(queryString: String,page: Int): Observable<List<Movie>>
}