package com.example.tgzoom.letswatch.domain.repository

import com.example.tgzoom.letswatch.domain.Trailer
import io.reactivex.MaybeObserver

/**
 * Created by tgzoom on 4/27/17.
 */
interface TrailerRepository {
    fun trailers(movieApiId: Long): MaybeObserver<List<Trailer>>
}