package com.example.tgzoom.letswatch.domain.scheduler

import io.reactivex.MaybeTransformer
import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler

/**
 * Created by tgzoom on 6/12/17.
 */
interface BaseScheduler {
    fun computation(): Scheduler

    fun io(): Scheduler

    fun ui(): Scheduler

    fun <T> applyObservableSchedulers(): ObservableTransformer<T,T>

    fun <T> applyMaybeSchedulers(): MaybeTransformer<T,T>
}