package com.example.tgzoom.letswatch.test.domain


import com.example.tgzoom.letswatch.domain.Movie
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.*
import org.junit.Test
import org.junit.Assert.*
import org.junit.Before

/**
 * Created by tgzoom on 4/22/17.
 */
class MovieTest {

    var movie: Movie = Movie()
    val FAKE_MOVIE_ID = 305L
    val FAKE_MOVIE_TITLE = "Pocahontas"

    @Before
    fun setUp(): Unit{
        movie.id = FAKE_MOVIE_ID
        movie.title = FAKE_MOVIE_TITLE
    }

    @Test
    @Throws(Exception::class)
    fun createdCorrectly(){
        assertThat(movie,CoreMatchers.notNullValue())
        assertThat(movie.id, equalTo(FAKE_MOVIE_ID))
        assertThat(movie.title, equalTo(FAKE_MOVIE_TITLE))
    }
}