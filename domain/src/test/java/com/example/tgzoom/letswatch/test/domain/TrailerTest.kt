package com.example.tgzoom.letswatch.test.domain

import com.example.tgzoom.letswatch.domain.Trailer
import org.hamcrest.CoreMatchers
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Created by tgzoom on 5/4/17.
 */
class TrailerTest {
    lateinit  var trailer: Trailer
    val FAKE_ID = "123456789"
    val FAKE_KEY = "123456789123456"

    @Before
    fun setUp(){
        trailer = Trailer(id = FAKE_ID, key = FAKE_KEY)
    }

    @Test
    fun createdCorrectly(){
        assertThat(trailer,CoreMatchers.notNullValue())
        assertThat(trailer.id,CoreMatchers.`is`(FAKE_ID))
        assertThat(trailer.key,CoreMatchers.`is`(FAKE_KEY))
    }
}