package com.example.tgzoom.letswatch.test.domain

import com.example.tgzoom.letswatch.domain.Video
import org.hamcrest.CoreMatchers.*
import org.junit.Assert
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Created by tgzoom on 4/22/17.
 */
class VideoTest {

    val video: Video = Video()
    val FAKE_VIDEO_ID:  String = "58f4d4129251413d76023ecf"
    val FAKE_VIDEO_KEY: String = "Kf2kk8T3Z9A"

    @Before
    fun setUp(): Unit{
        video.id = FAKE_VIDEO_ID
        video.key = FAKE_VIDEO_KEY
    }

    @Test
    @Throws(Exception::class)
    fun createdCorrectly(){
        assertThat(video, notNullValue())
        assertThat(video.id, equalTo(FAKE_VIDEO_ID))
        assertThat(video.key, equalTo(FAKE_VIDEO_KEY))
    }
}