package com.example.tgzoom.letswatch.test.domain.interactor

import com.example.tgzoom.letswatch.domain.Movie
import com.example.tgzoom.letswatch.domain.interactor.GetMovieDetailsUseCase
import com.example.tgzoom.letswatch.domain.repository.MovieRepository
import com.example.tgzoom.letswatch.domain.scheduler.BaseScheduler
import io.reactivex.MaybeObserver
import org.junit.Before
import org.junit.Test
import org.mockito.*
import org.mockito.Mockito.*

/**
 * Created by tgzoom on 4/26/17.
 */
class GetMovieDetailsUseCaseTest {

    @Mock lateinit var baseScheduler: BaseScheduler
    @Mock lateinit var movieRepository: MovieRepository
    @Mock lateinit var observer: MaybeObserver<Movie>
    @InjectMocks lateinit var getMovieDetails: GetMovieDetailsUseCase
    private val MOVIE_API_ID = 305L

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun methodsCallsTest(){
        getMovieDetails.execute(GetMovieDetailsUseCase.Request(MOVIE_API_ID,observer))
        verify(movieRepository).movie(MOVIE_API_ID)
        verifyNoMoreInteractions(movieRepository)
        verifyZeroInteractions(baseScheduler)
    }
}
