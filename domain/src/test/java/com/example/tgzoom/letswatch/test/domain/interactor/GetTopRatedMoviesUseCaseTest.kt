package com.example.tgzoom.letswatch.test.domain.interactor

import com.example.tgzoom.letswatch.domain.Movie
import com.example.tgzoom.letswatch.domain.interactor.GetTopRatedMoviesUseCase
import com.example.tgzoom.letswatch.domain.repository.MovieRepository
import com.example.tgzoom.letswatch.domain.scheduler.BaseScheduler
import io.reactivex.MaybeObserver
import org.junit.Before
import org.junit.Test
import org.mockito.*
import org.mockito.Mockito.*

/**
 * Created by tgzoom on 4/26/17.
 */
class GetTopRatedMoviesUseCaseTest {

    @Mock lateinit var baseScheduler: BaseScheduler
    @Mock lateinit var movieRepository: MovieRepository
    @Mock lateinit var observer: MaybeObserver<List<Movie>>
    @InjectMocks lateinit var getTopRatedMoviesUseCase: GetTopRatedMoviesUseCase
    private val FAKE_PAGE = 1

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun methodsCallsTest(){
        getTopRatedMoviesUseCase.execute(GetTopRatedMoviesUseCase.Request(FAKE_PAGE,observer))
        verify(movieRepository).topRated(FAKE_PAGE)
        verifyNoMoreInteractions(movieRepository)
        verifyZeroInteractions(baseScheduler)
    }
}