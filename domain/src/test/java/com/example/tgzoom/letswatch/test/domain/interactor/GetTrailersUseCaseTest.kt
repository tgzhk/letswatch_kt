package com.example.tgzoom.letswatch.test.domain.interactor

import com.example.tgzoom.letswatch.domain.interactor.GetTrailersUseCase
import com.example.tgzoom.letswatch.domain.repository.TrailerRepository
import com.example.tgzoom.letswatch.domain.scheduler.BaseScheduler
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

/**
 * Created by tgzoom on 5/4/17.
 */
class GetTrailersUseCaseTest {

    @Mock lateinit var baseScheduler: BaseScheduler
    @Mock lateinit var trailerRepository: TrailerRepository
    @InjectMocks lateinit var getTrailersUseCase: GetTrailersUseCase
    val FAKE_MOVIE_ID = 305L

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun methodsCallsTest(){
    }

}