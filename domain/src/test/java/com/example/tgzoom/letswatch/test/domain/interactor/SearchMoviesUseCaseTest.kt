package com.example.tgzoom.letswatch.test.domain.interactor

import com.example.tgzoom.letswatch.domain.Movie
import com.example.tgzoom.letswatch.domain.interactor.SearchMoviesUseCase
import com.example.tgzoom.letswatch.domain.repository.MovieRepository
import com.example.tgzoom.letswatch.domain.scheduler.BaseScheduler
import io.reactivex.MaybeObserver
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations


/**
 * Created by tgzoom on 5/1/17.
 */
class SearchMoviesUseCaseTest {

    @Mock lateinit var baseScheduler: BaseScheduler
    @Mock lateinit var movieRepository: MovieRepository
    @Mock lateinit var observer: MaybeObserver<List<Movie>>
    @InjectMocks lateinit var searchMoviesUseCase: SearchMoviesUseCase
    private var QUERY_STRING: String = "test"
    private val FAKE_PAGE = 1

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun observable_created(){
        searchMoviesUseCase.execute(SearchMoviesUseCase.Request(QUERY_STRING,FAKE_PAGE,observer))
        verify(movieRepository).search(QUERY_STRING,FAKE_PAGE)
        verifyNoMoreInteractions(movieRepository)
        verifyZeroInteractions(baseScheduler)
    }
}