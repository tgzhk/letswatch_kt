package com.example.tgzoom.letswatch.test.domain.interactor

import com.example.tgzoom.letswatch.domain.Movie
import com.example.tgzoom.letswatch.domain.interactor.UseCase
import com.example.tgzoom.letswatch.domain.scheduler.BaseScheduler
import io.reactivex.disposables.CompositeDisposable
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations


/**
 * Created by tgzoom on 4/23/17.
 */

class UseCaseTest {

    @Mock lateinit var baseScheduler: BaseScheduler
    @Mock lateinit var useCase: UseCase<UseCaseTest.Request>

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        useCase.mDisposable = CompositeDisposable()
    }

    @Test
    fun correctlySubscribing(){
        useCase.execute(Request)
        assertTrue(!useCase.mDisposable.isDisposed())
    }

    @Test
    fun correctlyUnsubscribing(){
        useCase.execute(Request)
        useCase.dispose()
        assertTrue(useCase.mDisposable.isDisposed())
    }

    object Request
    {

    }
}
